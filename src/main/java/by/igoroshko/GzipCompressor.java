package by.igoroshko;

import lombok.SneakyThrows;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPOutputStream;

public class GzipCompressor {

    @SneakyThrows
    public OutputStream compress(String string) {
        ByteArrayOutputStream object = new ByteArrayOutputStream();
        GZIPOutputStream gzip = new GZIPOutputStream(object);
        gzip.write(string.getBytes(StandardCharsets.UTF_8));
        gzip.close();
        return object;
    }
}
