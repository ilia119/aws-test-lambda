package by.igoroshko;

import lombok.SneakyThrows;
import software.amazon.awssdk.services.ssm.SsmAsyncClient;
import software.amazon.awssdk.services.ssm.model.GetParameterRequest;

public class ParameterFinder {

    @SneakyThrows
    public static String getSecureParameterFromSSM(String key) {
        SsmAsyncClient ssmClient = AwsClientsRegistry.getSsmClient();
        return ssmClient
                .getParameter(GetParameterRequest.builder()
                        .name(key)
                        .withDecryption(true)
                        .build())
                .get()
                .parameter()
                .value();
    }


}
