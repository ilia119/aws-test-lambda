package by.igoroshko;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class HttpRequestSender {

    public static final String POSTS_BY_USER_ID_URL = "/posts?userId=";

    public String sendRequest(String url, String userId) {
        HttpClient httpclient = HttpClient.newHttpClient();
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create(url + POSTS_BY_USER_ID_URL + userId))
                .build();

        return httpclient
                .sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body).join();
    }
}
