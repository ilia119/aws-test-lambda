package by.igoroshko;

import software.amazon.awssdk.core.async.AsyncRequestBody;
import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;
import software.amazon.awssdk.services.s3.model.PutObjectResponse;

import java.util.concurrent.CompletableFuture;

public class S3FileSender {

    public void sendToS3(byte[] bytes, String bucketName, String fileName) {
        S3AsyncClient client = AwsClientsRegistry.getS3AsyncClient();
        CompletableFuture<PutObjectResponse> future = client.putObject(
                PutObjectRequest.builder()
                        .bucket(bucketName)
                        .key(fileName)
                        .build(),
                AsyncRequestBody.fromBytes(bytes));

        future.whenComplete((resp, err) -> {
            try {
                if (resp != null) {
                    System.out.println("my response: " + resp);
                } else {
                    err.printStackTrace();
                }
            } finally {
                client.close();
            }
        });

        future.join();
    }
}
