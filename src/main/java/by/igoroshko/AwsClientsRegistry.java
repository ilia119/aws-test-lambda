package by.igoroshko;

import software.amazon.awssdk.services.s3.S3AsyncClient;
import software.amazon.awssdk.services.ssm.SsmAsyncClient;

public class AwsClientsRegistry {
    private static S3AsyncClient s3Client;
    private static SsmAsyncClient ssmClient;

    public static S3AsyncClient getS3AsyncClient() {
        if (s3Client == null) {
            return S3AsyncClient.create();
        }
        return s3Client;
    }

    public static SsmAsyncClient getSsmClient() {
        if (ssmClient == null) {
            return SsmAsyncClient.create();
        }
        return ssmClient;
    }
}
