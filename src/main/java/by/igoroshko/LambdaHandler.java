package by.igoroshko;

import by.igoroshko.model.Post;
import by.igoroshko.utils.UserPostRequest;
import by.igoroshko.utils.UserPostResponse;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public class LambdaHandler implements Function<UserPostRequest, UserPostResponse> {

    private HttpRequestSender requestSender = new HttpRequestSender();
    private GzipCompressor compressor = new GzipCompressor();
    private S3FileSender sender = new S3FileSender();
    private List<Post> posts = new ArrayList<>();
    private Logger logger = LoggerFactory.getLogger(LambdaHandler.class);

    //https://jsonplaceholder.typicode.com/posts

    @Override
    @SneakyThrows
    public UserPostResponse apply(UserPostRequest userPostRequest) {
        var userId = userPostRequest.getId();
        var fileName = "posts_" + userId + "_" + System.currentTimeMillis() + ".json.gz";

        var url = ParameterFinder.getSecureParameterFromSSM(System.getenv("urlParameterName"));
        var bucketName = ParameterFinder.getSecureParameterFromSSM(System.getenv("s3BucketParameterName"));

        logger.info("Base url from ssm - " + url);
        logger.info("S3 Bucket Name from ssm - " + bucketName);

        var stringResponse = requestSender.sendRequest(url, userId);
        posts = new ObjectMapper().readValue(stringResponse, new TypeReference<List<Post>>() {
        });
        if (posts.size() == 0) {
            return UserPostResponse.NO_POSTS;
        }

        ByteArrayOutputStream compressedFile = (ByteArrayOutputStream) compressor.compress(stringResponse);

        sender.sendToS3(compressedFile.toByteArray(), bucketName, fileName);

        return UserPostResponse.SUCCESS;
    }
}
